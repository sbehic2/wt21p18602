let assert = chai.assert;
describe('TestoviParser', function() {
    describe('dajTacnost()', function() {
        it('Test u kojem prolaze svi testovi - tacnost 100%', function() {
            var test = TestoviParser.dajTacnost(`{
                "stats":{
                    "suites":2,
                    "tests":2,
                    "passes":2,
                    "pending":0,
                    "failures":0,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"Ovo je prvi test",
                        "fullTitle":"Ovo je prvi test i on prolazi",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"Ovo je drugi test",
                        "fullTitle":"Ovo je drugi test i on prolazi",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[],
                "passes":[
                    {
                        "title":"Ovo je prvi test",
                        "fullTitle":"Ovo je prvi test i on prolazi",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"Ovo je drugi test",
                        "fullTitle":"Ovo je drugi test i on prolazi",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ]
            }`);

            assert.equal(JSON.stringify(test), '{"tacnost":"100%","greske":[]}');
        });


        it('Test u kojem prolazi samo jedan test od cetiri - tacnost 25%', function() {
            var test = TestoviParser.dajTacnost(`{
                "stats":{
                    "suites":4,
                    "tests":4,
                    "passes":1,
                    "pending":0,
                    "failures":3,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"Test 1",
                        "fullTitle":"Ovo je Test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"Test 2",
                        "fullTitle":"Ovo je Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"Test 3",
                        "fullTitle":"Ovo je Test 3",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"Test 4",
                        "fullTitle":"Ovo je Test 4",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[
                    {
                        "title":"Test 2",
                        "fullTitle":"Ovo je Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"Test 3",
                        "fullTitle":"Ovo je Test 3",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"Test 4",
                        "fullTitle":"Ovo je Test 4",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "passes":[
                    {
                        "title":"Test 1",
                        "fullTitle":"Ovo je Test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ]
                 }`);
            assert.equal(JSON.stringify(test), '{"tacnost":"25%","greske":["Ovo je Test 2","Ovo je Test 3","Ovo je Test 4"]}');
        });
        it('Test u kojem padaju svi testovi - tacnost 0%', function() {
            var test = TestoviParser.dajTacnost(`{
                "stats":{
                    "suites":2,
                    "tests":2,
                    "passes":0,
                    "pending":0,
                    "failures":2,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"Ovo je prvi test",
                        "fullTitle":"Ovo je prvi test i on pada",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"Ovo je drugi test",
                        "fullTitle":"Ovo je drugi test i on padai",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[ 
                    {
                        "title":"Ovo je prvi test",
                        "fullTitle":"Ovo je prvi test i on pada",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"Ovo je drugi test",
                        "fullTitle":"Ovo je drugi test i on pada",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }],
                "passes":[]
            }`);

            assert.equal(JSON.stringify(test), '{"tacnost":"0%","greske":["Ovo je prvi test i on pada","Ovo je drugi test i on pada"]}');
        });

        it('U funkciju poslan nevalidan JSON string - Testovi se ne mogu izvrsiti', function() {
            var test = TestoviParser.dajTacnost("Ovo nece raditi jer ovo nije JSON string");
            assert.equal(JSON.stringify(test), '{"tacnost":"0%","greske":["Testovi se ne mogu izvršiti"]}');
        });

        it('Test u kojem tacnost nije cijeli broj - prolaze dva od tri testa - tacnost 66.7%', function() {
            var test = TestoviParser.dajTacnost(`{
                "stats":{
                    "suites":3,
                    "tests":3,
                    "passes":2,
                    "pending":0,
                    "failures":1,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"Ovo je prvi test",
                        "fullTitle":"Ovo je prvi test i on prolazi",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"Ovo je drugi test",
                        "fullTitle":"Ovo je drugi test i on prolazi",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"Ovo je treci test",
                        "fullTitle":"Ovo je treci test i on pada",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[
                    {
                        "title":"Ovo je treci test",
                        "fullTitle":"Ovo je treci test i on pada",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "passes":[
                    {
                        "title":"Ovo je prvi test",
                        "fullTitle":"Ovo je prvi test i on prolazi",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"Ovo je drugi test",
                        "fullTitle":"Ovo je drugi test i on prolazi",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ]
            }`);

            assert.equal(JSON.stringify(test), '{"tacnost":"66.7%","greske":["Ovo je treci test i on pada"]}');
        });

    });
});