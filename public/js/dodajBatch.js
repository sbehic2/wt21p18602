document.getElementById("btnPosalji").addEventListener('click', function(event) {
    document.getElementById("ajaxstatus").innerHTML = '';
    var podaci = document.getElementById('podaci').value;
    if (podaci == undefined)
        podaci = '';

    //provjera podataka
    var redovi = podaci.split('\n');
    redovi = redovi.filter(r => r.length != 0);
    var greska = false;
    for (let i = 0; i < redovi.length; i++) {
        let podatak = redovi[i].split(',');
        if (podatak.length != 4 || podatak[0].length == 0 || podatak[1].length == 0 || podatak[2].length == 0 || podatak[3].length == 0) {
            greska = true;
        }
    }

    if (greska == true)
        document.getElementById("ajaxstatus").innerHTML = "Niste unijeli ispravan CSV format ili neki podaci nedostaju! Provjerite unesene podatke!";

    if (!greska) {
        StudentAjax.dodajBatch(podaci, (err, data) => {
            if (data != null)
                document.getElementById("ajaxstatus").innerHTML = data.status;
            else {
                document.getElementById("ajaxstatus").innerHTML = err;
            }

        });
    }

});