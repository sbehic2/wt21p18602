var TestoviParser = (function() {
    var dajTacnost = function(jsonIzvjestaj) {
        let objekat;
        var izvjestaj = {};

        try {
            objekat = JSON.parse(jsonIzvjestaj);

            if (!objekat.hasOwnProperty('stats') || !objekat.stats.hasOwnProperty('tests') || !objekat.stats.hasOwnProperty('passes') || !objekat.hasOwnProperty('failures')) {
                throw new TypeError("Nedostaju polja");
            }
            var ukupnoTestova = parseInt(objekat.stats.tests);
            var brojUspjesnihTestova = parseInt(objekat.stats.passes);

            var tacnost = (brojUspjesnihTestova / ukupnoTestova * 100);

            if ((parseInt((tacnost).toFixed(1) * 10) % 10) != 0) {
                tacnost = (tacnost).toFixed(1);
            }

            izvjestaj.tacnost = tacnost + "%";
            var nizGresaka = [];

            for (let i = 0; i < objekat.failures.length; i++) {
                if (!objekat.failures[i].hasOwnProperty('fullTitle')) {
                    throw new TypeError("Nedostaju polja");
                }
                nizGresaka[i] = objekat.failures[i].fullTitle;
            }

            izvjestaj.greske = nizGresaka;
        } catch (e) {
            izvjestaj.tacnost = "0%";
            izvjestaj.greske = ["Testovi se ne mogu izvršiti"];
        }
        return izvjestaj;
    };

    var porediRezultate = function(rezultat1, rezultat2) {

        let objekat1, objekat2;
        var povratni = {};
        try {
            objekat1 = JSON.parse(rezultat1);
            objekat2 = JSON.parse(rezultat2);

            var niz1 = [];
            var niz2 = [];

            if (!objekat1.hasOwnProperty('stats') || !objekat1.stats.hasOwnProperty('tests') || !objekat1.stats.hasOwnProperty('passes') || !objekat1.hasOwnProperty('failures') || !objekat1.hasOwnProperty('tests')) {
                throw new TypeError("Nedostaju polja");
            }
            if (!objekat2.hasOwnProperty('stats') || !objekat2.stats.hasOwnProperty('tests') || !objekat2.stats.hasOwnProperty('passes') || !objekat2.hasOwnProperty('failures') || !objekat2.hasOwnProperty('tests')) {
                throw new TypeError("Nedostaju polja");
            }

            if (objekat1.tests.length == objekat2.tests.length) {
                for (let i = 0; i < objekat1.tests.length; i++) {
                    if (!objekat1.tests[i].hasOwnProperty('fullTitle') || !objekat2.tests[i].hasOwnProperty('fullTitle')) {
                        throw new TypeError("Nedostaju polja");
                    }
                    niz1[i] = objekat1.tests[i].fullTitle;
                    niz2[i] = objekat2.tests[i].fullTitle;
                }

                niz1.sort();
                niz2.sort();

                if (JSON.stringify(niz1) === JSON.stringify(niz2)) {
                    povratni.promjena = dajTacnost(rezultat2).tacnost;
                    povratni.greske = dajTacnost(rezultat2).greske.sort();
                    return povratni;
                }
            }

            let filtriran;
            let testovi1KojiPadaju = [];
            for (let i = 0; i < objekat1.failures.length; i++) {
                if (!objekat1.failures[i].hasOwnProperty('fullTitle')) {
                    throw new TypeError("Nedostaju polja");
                }
                testovi1KojiPadaju[i] = objekat1.failures[i].fullTitle;
            }

            let testovi2KojiPadaju = [];
            for (let i = 0; i < objekat2.failures.length; i++) {
                if (!objekat2.failures[i].hasOwnProperty('fullTitle')) {
                    throw new TypeError("Nedostaju polja");
                }
                testovi2KojiPadaju[i] = objekat2.failures[i].fullTitle;
            }

            for (let i = 0; i < objekat2.tests.length; i++) {
                if (!objekat2.tests[i].hasOwnProperty('fullTitle')) {
                    throw new TypeError("Nedostaju polja");
                }
                niz2[i] = objekat2.tests[i].fullTitle;
            }
            filtriran = testovi1KojiPadaju.filter(function f(value) { return !niz2.includes(value); }).sort();
            povratni.promjena = (filtriran.length + objekat2.failures.length) / (filtriran.length + objekat2.tests.length) * 100;
            if ((parseInt((povratni.promjena).toFixed(1) * 10) % 10) != 0) {
                povratni.promjena = (povratni.promjena).toFixed(1);
            }
            povratni.promjena = povratni.promjena + "%";

            var filtriran2 = testovi2KojiPadaju.sort();

            povratni.greske = filtriran.concat(filtriran2);


        } catch (e) {
            povratni.promjena = "0%";
            povratni.greske = ["Testovi se ne mogu izvršiti"];

        }
        return povratni;
    }

    return {
        dajTacnost: dajTacnost,
        porediRezultate: porediRezultate
    }
}());