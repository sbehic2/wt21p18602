var VjezbeAjax = (function() {
    //0-brojVjezbi - je li od 1 do brojVjezbi ukljucivo
    var dodajInputPolja = function(DOMelementDIVauFormi, brojVjezbi) {
        let string = '';
        if (Number.isInteger(brojVjezbi) && brojVjezbi >= 1 && brojVjezbi <= 15) {
            for (let i = 0; i < brojVjezbi; i++) {
                let br = i + 1;
                string += '<label for="z' + i + '">Broj zadataka u vježbi ' + br + ':</label><input type="number" value="4" name=z' + i + ' id=z' + i + '>'
            }
        } else {
            console.log("Unijeli ste nevalidan broj vjezbi!");
        }
        if (DOMelementDIVauFormi != null)
            DOMelementDIVauFormi.innerHTML = string;
        else {
            console.log('Niste poslali referencu na element!');
        }
    };

    var posaljiPodatke = function(vjezbeObjekat, callbackFja) {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() { // Anonimna funkcija

            var err = null;
            var data = null;
            if (ajax.readyState == 4 && ajax.status == 200) {
                try {
                    var obj = JSON.parse(ajax.responseText);
                } catch (error) {
                    callbackFja("Nije ispravan JSON!", null);
                    return;
                }
                if (obj.hasOwnProperty('status')) {
                    err = obj['data'];
                } else {
                    let porukaGreske = 'Pogrešan parametar ';
                    let imaGreske = false;
                    if (!Number.isInteger(obj['brojVjezbi']) || obj['brojVjezbi'] < 1 || obj['brojVjezbi'] > 15) {
                        imaGreske = true;
                        porukaGreske += 'brojVjezbi';
                    }
                    if (obj['brojZadataka'] != null) {
                        for (let i = 0; i < obj['brojZadataka'].length; i++) {
                            if (!Number.isInteger(obj['brojZadataka'][i]) || obj['brojZadataka'][i] < 0 || obj['brojZadataka'][i] > 10) {
                                if (imaGreske)
                                    porukaGreske += ',';
                                imaGreske = true;
                                porukaGreske += 'z' + i;
                            }
                        }
                        if (obj['brojZadataka'].length != obj['brojVjezbi']) {
                            if (imaGreske)
                                porukaGreske += ',';
                            porukaGreske += 'brojZadataka';
                            imaGreske = true;
                        }
                    } else {
                        if (imaGreske)
                            porukaGreske += ',';
                        porukaGreske += 'brojZadataka';
                        imaGreske = true;
                    }
                    if (imaGreske) {
                        err = porukaGreske;
                    } else
                        data = obj;
                }

                callbackFja(err, data);
            }
            if (ajax.readyState == 4 && ajax.status == 404)
                console.log("Greska: nepoznat URL");
        }
        ajax.open("POST", "http://localhost:3000/vjezbe", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(vjezbeObjekat));
    }

    var dohvatiPodatke = function(callbackFja) {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() { // Anonimna funkcija
            var err = null;
            var data = null;
            if (ajax.readyState == 4 && ajax.status == 200) {
                try {
                    var obj = JSON.parse(ajax.responseText);
                } catch (error) {
                    callbackFja("Nije ispravan JSON!", null);
                    return;
                }
                let porukaGreske = 'Pogrešan parametar ';
                let imaGreske = false;
                if (!Number.isInteger(obj['brojVjezbi']) || obj['brojVjezbi'] < 1 || obj['brojVjezbi'] > 15) {
                    imaGreske = true;
                    porukaGreske += 'brojVjezbi';
                }
                if (obj['brojZadataka'] != null) {
                    for (let i = 0; i < obj['brojZadataka'].length; i++) {
                        if (!Number.isInteger(obj['brojZadataka'][i]) || obj['brojZadataka'][i] < 0 || obj['brojZadataka'][i] > 10) {
                            if (imaGreske)
                                porukaGreske += ',';
                            imaGreske = true;
                            porukaGreske += 'z' + i;
                        }
                    }
                    if (obj['brojZadataka'].length != obj['brojVjezbi']) {
                        if (imaGreske)
                            porukaGreske += ',';
                        porukaGreske += 'brojZadataka';
                        imaGreske = true;
                    }
                } else {
                    if (imaGreske)
                        porukaGreske += ',';
                    porukaGreske += 'brojZadataka';
                    imaGreske = true;
                }
                if (imaGreske) {
                    err = porukaGreske;
                } else
                    data = obj;
                callbackFja(err, data);
            }
            if (ajax.readyState == 4 && ajax.status == 404)
                console.log("Greska: nepoznat URL");
        }
        ajax.open("GET", "http://localhost:3000/vjezbe", true);
        ajax.send();
    }

    var iscrtajVjezbe = function(divDOMelement, vjezbeObjekat) {
        //u slucaju pogresnih podataka, kreirat cemo istu gresku kao u prethodnom zadatku
        let porukaGreske = 'Pogrešan parametar ';
        let imaGreske = false;
        if (!Number.isInteger(vjezbeObjekat['brojVjezbi']) || vjezbeObjekat['brojVjezbi'] < 1 || vjezbeObjekat['brojVjezbi'] > 15) {
            porukaGreske += 'brojVjezbi';
            imaGreske = true;
        }
        for (let i = 0; i < vjezbeObjekat['brojZadataka'].length; i++) {
            if (!Number.isInteger(vjezbeObjekat['brojZadataka'][i]) || vjezbeObjekat['brojZadataka'][i] < 0 || vjezbeObjekat['brojZadataka'][i] > 10) {
                if (imaGreske)
                    porukaGreske += ',';
                imaGreske = true;
                porukaGreske += 'z' + i;
            }
        }
        if (vjezbeObjekat['brojVjezbi'] != vjezbeObjekat['brojZadataka'].length) {
            if (imaGreske)
                porukaGreske += ',';
            porukaGreske += 'brojZadataka';
            imaGreske = true;
        }

        if (imaGreske) {
            let greska = {};
            greska['status'] = 'error';
            greska['data'] = porukaGreske;
            return;
        }

        let string = '';
        for (let i = 1; i < vjezbeObjekat['brojVjezbi'] + 1; i++) {
            string += '<div class="vjezbe"><input type="button" onClick="VjezbeAjax.iscrtajZadatke(this.parentElement,' + vjezbeObjekat['brojZadataka'][i - 1] + ')" class="vjezbaDugme" value="VJEŽBA ' + i + '"></div>'
        }
        if (divDOMelement != null)
            divDOMelement.innerHTML = string;
        else
            console.log('Niste poslali referencu na element!');
    }

    var iscrtani = [];
    var prikazani = null;
    var buttonPrikazani = [];

    var iscrtajZadatke = function(vjezbaDOMelement, brojZadataka) {

        if (vjezbaDOMelement == null)
            console.log('Niste poslali referencu na element!');
        if (!Number.isInteger(brojZadataka) || brojZadataka < 0 || brojZadataka > 10) {
            console.log("Neispravan broj zadataka za datu vjezbu.");
            return;
        }
        if (vjezbaDOMelement == null)
            return;

        if (prikazani != null) {
            prikazani.classList.remove('vjezbeSaZadacima');
            prikazani.classList.add('vjezbe');
            let index = iscrtani.indexOf(prikazani);
            for (let j = 0; j < buttonPrikazani[index].length; j++)
                buttonPrikazani[index][j].style['display'] = 'none';
        }
        if (!iscrtani.includes(vjezbaDOMelement)) {
            let buttonPrikazani1 = [];
            for (let i = 1; i < brojZadataka + 1; i++) {
                let element = document.createElement('input');
                element.type = 'button';
                element.classList.add('zadatakDugme');
                element.value = 'ZADATAK ' + i;
                buttonPrikazani1.push(element);
                vjezbaDOMelement.appendChild(element);
            }
            buttonPrikazani.push(buttonPrikazani1);
            iscrtani.push(vjezbaDOMelement);
            prikazani = vjezbaDOMelement;

        } else {
            //metoda je vec bila pozvana, ali sad je druga vrijednost paramettra brojZadataka
            let index = iscrtani.indexOf(prikazani);
            for (let j = 0; j < buttonPrikazani[index].length; j++)
                buttonPrikazani[index][j].style['display'] = 'none';
            prikazani = vjezbaDOMelement;

            index = iscrtani.indexOf(prikazani);
            if (buttonPrikazani[index].length != brojZadataka) {
                //u ovom slucaju se mijenja broj zadataka za neku vec prikazanu vjezbu
                //u tom slucaju trebamo ponovo iscrtati zadatke za tu vjezbu
                let brDjece = prikazani.children.length;
                for (let i = 0; i < brDjece - 1; i++) {
                    //jer nam je prvo dijet eu biti zadatak, pa brisemo sve osim njega  
                    prikazani.removeChild(prikazani.lastChild);
                }
                let buttonPrikazani1 = [];
                for (let i = 1; i < brojZadataka + 1; i++) {
                    let element = document.createElement('input');
                    element.type = 'button';
                    element.classList.add('zadatakDugme');
                    element.value = 'ZADATAK ' + i;
                    buttonPrikazani1.push(element);
                    vjezbaDOMelement.appendChild(element);
                }

                buttonPrikazani.splice(index, 1);
                buttonPrikazani.push(buttonPrikazani1);

                iscrtani.splice(index, 1);
                prikazani = vjezbaDOMelement;
                iscrtani.push(prikazani);

            }

            index = iscrtani.indexOf(prikazani);
            for (let j = 0; j < buttonPrikazani[index].length; j++)
                buttonPrikazani[index][j].style['display'] = 'inline';



        }
        vjezbaDOMelement.classList.remove('vjezbe');
        vjezbaDOMelement.classList.add('vjezbeSaZadacima');

    }
    return {
        dodajInputPolja: dodajInputPolja,
        posaljiPodatke: posaljiPodatke,
        dohvatiPodatke: dohvatiPodatke,
        iscrtajVjezbe: iscrtajVjezbe,
        iscrtajZadatke: iscrtajZadatke
    }
}());