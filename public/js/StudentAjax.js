var StudentAjax = (function() {
    var dodajStudenta = function(student, fnCallBack) {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                try {
                    var err = null;
                    var data = null;
                    var obj = JSON.parse(ajax.responseText);
                    if (obj.hasOwnProperty('status')) {
                        if (obj['status'] === "Kreiran student!") {
                            data = obj;
                        } else {
                            err = obj['status'];
                        }
                        fnCallBack(err, data);
                    }
                } catch (error) {
                    callbackFja("Nije ispravan JSON!", null);
                    return;
                }
            }
            if (ajax.readyState == 4 && ajax.status == 404)
                console.log("Greska: nepoznat URL");
        }
        ajax.open("POST", "http://localhost:3000/student", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(student));
    }
    var postaviGrupu = function(index, grupa, fnCallBack) {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                try {
                    var err = null;
                    var data = null;
                    var obj = JSON.parse(ajax.responseText);
                    if (obj.hasOwnProperty('status')) {
                        if (obj['status'] === ("Promjenjena grupa studentu " + index)) {
                            data = obj;
                        } else {
                            err = obj['status'];
                        }
                        fnCallBack(err, data);
                    }
                } catch (error) {
                    callbackFja("Nije ispravan JSON!", null);
                    return;
                }
            }
            if (ajax.readyState == 4 && ajax.status == 404)
                console.log("Greska: nepoznat URL");
        }
        ajax.open("PUT", "http://localhost:3000/student/" + index, true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify({ "grupa": grupa }));
    }

    var dodajBatch = function(csvStudenti, fnCallBack) {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {
                try {
                    var err = null;
                    var data = null;
                    var obj = JSON.parse(ajax.responseText);
                    if (obj.hasOwnProperty('status')) {
                        if (obj['status'] == "Niste unijeli ispravan CSV format! Provjerite unesene podatke!")
                            err = obj['status'];
                        else
                            data = obj;
                        fnCallBack(err, data);
                    }
                } catch (error) {
                    callbackFja("Nije ispravan JSON!", null);
                    return;
                }
            }
            if (ajax.readyState == 4 && ajax.status == 404)
                console.log("Greska: nepoznat URL");
        }
        ajax.open("POST", "http://localhost:3000/batch/student", true);
        ajax.setRequestHeader("Content-Type", "text/plain");
        ajax.send(csvStudenti);
    }
    return {
        dodajStudenta: dodajStudenta,
        postaviGrupu: postaviGrupu,
        dodajBatch: dodajBatch
    }
}());