document.getElementById("btnPosalji").addEventListener('click', function(event) {
    document.getElementById("ajaxstatus").innerHTML = '';
    var index = document.getElementById('index').value;
    var grupa = document.getElementById('grupa').value;

    var greska = false;
    if (index === '' || index == null || index == undefined) {
        console.log("Morate unijeti ime studenta!");
        greska = true;
    }
    if (grupa == '' || grupa == null || grupa == undefined) {
        console.log("Morate unijeti prezime studenta!");
        greska = true;
    }

    if (greska) {
        document.getElementById("ajaxstatus").innerHTML = "Niste unijeli sve podatke!";
    }

    if (!greska) {
        StudentAjax.postaviGrupu(index, grupa, (err, data) => {
            if (data == null)
                document.getElementById("ajaxstatus").innerHTML = err;
            else {
                document.getElementById("ajaxstatus").innerHTML = data.status;
            }
        });
    }

});