let assert = chai.assert;

describe('VjezbeAjax', function() {
    describe('dodajInputPolja()', function() {
        it('Provjera broja dodanih input polja', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';
            VjezbeAjax.dodajInputPolja(pomocniDiv, 2);
            var brojPolja = 0;
            //ispred svakog inputa treba postojati labela, zato nju preskacemo
            for (let i = 0; i < pomocniDiv.children.length; i += 2) {
                brojPolja++;
            }
            pomocniDiv.innerHTML = '';
            assert.equal(brojPolja, 2, 'Broj input polja nije dva!');
        });

        it('Provjera koji id imaju dodana input polja', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';
            VjezbeAjax.dodajInputPolja(pomocniDiv, 3);
            //ispred svakog inputa treba postojati labela, zato nju preskacemo
            for (let i = 0; i < pomocniDiv.children.length; i += 2) {
                let id = i / 2;
                id = 'z' + id;
                assert.equal(pomocniDiv.children[i + 1].id === id, true, 'Dodana input polja moraju imati id odgovarajuceg oblika z0, z1 ...');
            }
            pomocniDiv.innerHTML = '';
        });

        it('Provjera koji name imaju dodana input polja', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';
            //ispred svakog inputa treba postojati labela, zato nju preskacemo
            for (let i = 0; i < pomocniDiv.children.length; i += 2) {
                let name = i / 2;
                name = 'z' + name;
                assert.equal(pomocniDiv.children[i + 1].name === name, true, 'Dodana input polja moraju imati name odgovarajuceg oblika z0, z1 ...');
            }
            pomocniDiv.innerHTML = '';
        });

        it('Dodavanje input polja u div koji vec posjeduje input polja', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';
            VjezbeAjax.dodajInputPolja(pomocniDiv, 2);
            VjezbeAjax.dodajInputPolja(pomocniDiv, 3);
            var brojPolja = 0;
            //ispred svakog inputa treba postojati labela, zato nju preskacemo
            for (let i = 0; i < pomocniDiv.children.length; i += 2) {
                brojPolja++;
            }
            assert.equal(brojPolja, 3, 'Broj dodanih polja treba odgovarati drugom parametru posljednjeg poziva funckija, i iznosi 3 trenutno!');
            pomocniDiv.innerHTML = '';
        });

        it('Provjera da li input polja posjeduju defaultnu vrijednost', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';
            VjezbeAjax.dodajInputPolja(pomocniDiv, 3);
            var value1 = document.getElementById('z0').value;
            var value2 = document.getElementById('z1').value;
            var value3 = document.getElementById('z2').value;
            assert.equal(value1, 4, 'Defaultna vrijednost za svako input polje treba biti 4!');
            assert.equal(value2, 4, 'Defaultna vrijednost za svako input polje treba biti 4!');
            assert.equal(value3, 4, 'Defaultna vrijednost za svako input polje treba biti 4!');
            pomocniDiv.innerHTML = '';
        });
        it('Broj vjezbi je prazan string ili null', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';
            VjezbeAjax.dodajInputPolja(pomocniDiv, null);
            assert.equal(pomocniDiv.innerHTML, '');
            pomocniDiv.innerHTML = '';
            VjezbeAjax.dodajInputPolja(pomocniDiv, '');
            assert.equal(pomocniDiv.innerHTML, '');
            pomocniDiv.innerHTML = '';
        });
        it('Broj vjezbi je negativan broj', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';
            VjezbeAjax.dodajInputPolja(pomocniDiv, -10);
            assert.equal(pomocniDiv.innerHTML, '', 'U slucaju nevalidnog broja vjezbi, ne iscrtavamo input polja');
            pomocniDiv.innerHTML = '';
        });

        it('Broj vjezbi je 0 ili veci od 15', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';
            VjezbeAjax.dodajInputPolja(pomocniDiv, 0);
            assert.equal(pomocniDiv.innerHTML, '', 'U slucaju nevalidnog broja vjezbi, ne iscrtavamo input polja');
            pomocniDiv.innerHTML = '';
            VjezbeAjax.dodajInputPolja(pomocniDiv, 16);
            assert.equal(pomocniDiv.innerHTML, '', 'U slucaju nevalidnog broja vjezbi, ne iscrtavamo input polja');
            pomocniDiv.innerHTML = '';
        });
        it('Provjera granicnih legalnih vrijednosti za broj vjezbi', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';
            VjezbeAjax.dodajInputPolja(pomocniDiv, 1);
            var brojPolja = 0;
            //ispred svakog inputa treba postojati labela, zato nju preskacemo
            for (let i = 0; i < pomocniDiv.children.length; i += 2) {
                brojPolja++;
            }
            assert.equal(brojPolja, 1, 'Broj input polja treba biti 1');
            pomocniDiv.innerHTML = '';
            VjezbeAjax.dodajInputPolja(pomocniDiv, 15);
            brojPolja = 0;
            for (let i = 0; i < pomocniDiv.children.length; i += 2) {
                brojPolja++;
            }
            assert.equal(brojPolja, 15, 'Broj input polja treba biti 15');
            pomocniDiv.innerHTML = '';
        });
        it('Broj vjezbi nije cijeli broj', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';
            VjezbeAjax.dodajInputPolja(pomocniDiv, 12.5);
            assert.equal(pomocniDiv.innerHTML, '', 'U slucaju nevalidnog broja vjezbi, ne iscrtavamo input polja');
            pomocniDiv.innerHTML = '';
        });
    });

    describe('iscrtajVjezbe()', function() {
        it('Parametar funkcije je objekat sa validnim podacima o broju vjezbi i zadataka', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';
            VjezbeAjax.iscrtajVjezbe(pomocniDiv, { "brojVjezbi": 3, "brojZadataka": [1, 2, 3] });
            assert.equal(pomocniDiv.childElementCount, 3);
            VjezbeAjax.iscrtajVjezbe(pomocniDiv, { "brojVjezbi": 2, "brojZadataka": [1, 2] });
            assert.equal(pomocniDiv.childElementCount, 2);
            pomocniDiv.innerHTML = '';
        });
        it('Negativan broj vjezbi ili broj vjezbi necijeli broj', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';
            VjezbeAjax.iscrtajVjezbe(pomocniDiv, { "brojVjezbi": -5, "brojZadataka": [] });
            assert.equal(pomocniDiv.childElementCount, 0);
            assert.equal(pomocniDiv.innerHTML, '');
            VjezbeAjax.iscrtajVjezbe(pomocniDiv, { "brojVjezbi": 2.5, "brojZadataka": [1, 2] });
            assert.equal(pomocniDiv.childElementCount, 0);
            assert.equal(pomocniDiv.innerHTML, '');
            pomocniDiv.innerHTML = '';
        });
        it('Broj vjezbi veci od 15', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';
            VjezbeAjax.iscrtajVjezbe(pomocniDiv, { "brojVjezbi": 20, "brojZadataka": [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1] });
            assert.equal(pomocniDiv.childElementCount, 0);
            assert.equal(pomocniDiv.innerHTML, '');
        });
        it('Nevalidna velicina niza brojZadataka u odnosu na broj vjezbi', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';
            VjezbeAjax.iscrtajVjezbe(pomocniDiv, { "brojVjezbi": 5, "brojZadataka": [1, 2, 3] });
            assert.equal(pomocniDiv.childElementCount, 0);
            assert.equal(pomocniDiv.innerHTML, '');
        });
        it('Nevalidna vrijednost broja zadataka na pojedinim vjezbama', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';
            VjezbeAjax.iscrtajVjezbe(pomocniDiv, { "brojVjezbi": 3, "brojZadataka": [-1, 2, 3] });
            assert.equal(pomocniDiv.childElementCount, 0);
            assert.equal(pomocniDiv.innerHTML, '');
            pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';
            VjezbeAjax.iscrtajVjezbe(pomocniDiv, { "brojVjezbi": 3, "brojZadataka": [1, 2.5, 3] });
            assert.equal(pomocniDiv.childElementCount, 0);
            assert.equal(pomocniDiv.innerHTML, '');
            pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';
            VjezbeAjax.iscrtajVjezbe(pomocniDiv, { "brojVjezbi": 3, "brojZadataka": [1, 2, 11] });
            assert.equal(pomocniDiv.childElementCount, 0);
            assert.equal(pomocniDiv.innerHTML, '');
        });
        it('Testiranje granicnih validnih vrijednosti za brojVjezbi', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';
            VjezbeAjax.iscrtajVjezbe(pomocniDiv, { "brojVjezbi": 1, "brojZadataka": [1] });
            assert.equal(pomocniDiv.childElementCount, 1);
            pomocniDiv.innerHTML = '';
            VjezbeAjax.iscrtajVjezbe(pomocniDiv, { "brojVjezbi": 15, "brojZadataka": [1, 2, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1] });
            assert.equal(pomocniDiv.childElementCount, 15);
            pomocniDiv.innerHTML = '';
        })
    });

    describe('iscrtajZadatke()', function() {
        //funckiji saljemo div u kojem se nalazi dugme Vjezba, tu se dodaju dugmad za zadatke, dakle sve se nalazi u jednom vanjskom divu
        it('Iscrtavanje zadataka pri validnim podacima za jednu vjezbu', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';

            var vjezba1 = document.createElement('button');
            pomocniDiv.appendChild(vjezba1);
            VjezbeAjax.iscrtajZadatke(pomocniDiv, 7);
            assert.equal(pomocniDiv.childElementCount, 8);
            pomocniDiv.innerHTML = '';
        });

        it('Broj zadataka manji od 0', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';

            var vjezba1 = document.createElement('button');
            pomocniDiv.appendChild(vjezba1);
            VjezbeAjax.iscrtajZadatke(pomocniDiv, -10);
            assert.equal(pomocniDiv.childElementCount, 1);
            pomocniDiv.innerHTML = '';
        });

        it('Broj zadataka veci od 10', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';

            var vjezba1 = document.createElement('button');
            pomocniDiv.appendChild(vjezba1);
            VjezbeAjax.iscrtajZadatke(pomocniDiv, 15);
            assert.equal(pomocniDiv.childElementCount, 1);
            pomocniDiv.innerHTML = '';
        });

        it('Broj zadataka je necijeli broj', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';

            var vjezba1 = document.createElement('button');
            pomocniDiv.appendChild(vjezba1);
            VjezbeAjax.iscrtajZadatke(pomocniDiv, 10.5);
            assert.equal(pomocniDiv.childElementCount, 1);
            pomocniDiv.innerHTML = '';
        });

        it('Broj zadataka predstavlja granicne validne vrijednosti (0 i 10)', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';

            var vjezba1 = document.createElement('button');
            pomocniDiv.appendChild(vjezba1);
            VjezbeAjax.iscrtajZadatke(pomocniDiv, 10);
            assert.equal(pomocniDiv.childElementCount, 11);
            pomocniDiv.innerHTML = '';
            var vjezba2 = document.createElement('button');
            pomocniDiv.appendChild(vjezba2);
            VjezbeAjax.iscrtajZadatke(pomocniDiv, 0);
            assert.equal(pomocniDiv.childElementCount, 1);
            pomocniDiv.innerHTML = '';
        });

        it('Test prikaza i skrivanja vjezbi', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';

            var pomocniDivVjezba1 = document.createElement('div');
            pomocniDiv.appendChild(pomocniDivVjezba1);
            var pomocniDivVjezba2 = document.createElement('div');
            pomocniDiv.appendChild(pomocniDivVjezba2);

            var vjezba1 = document.createElement('button');
            pomocniDivVjezba1.appendChild(vjezba1);
            vjezba1.addEventListener("click", function() { VjezbeAjax.iscrtajZadatke(pomocniDivVjezba1, 2); })
            vjezba1.innerText = "VJEŽBA 1";
            var vjezba2 = document.createElement('button');
            pomocniDivVjezba2.appendChild(vjezba2);
            vjezba2.addEventListener("click", function() { VjezbeAjax.iscrtajZadatke(pomocniDivVjezba2, 3); })
            vjezba2.innerText = "VJEŽBA 2";

            vjezba1.click();
            //brojimo jedno dijete koje je vjezba + broj zadataka za tu vjezbu
            assert.equal(pomocniDivVjezba1.childElementCount, 3);
            assert.equal(pomocniDivVjezba2.childElementCount, 1);

            vjezba2.click();
            assert.equal(pomocniDivVjezba1.childElementCount, 3);
            assert.equal(pomocniDivVjezba2.childElementCount, 4);

            for (let i = 1; i < pomocniDivVjezba1.childElementCount; i++) {
                assert.equal(pomocniDivVjezba1.children[i].style['display'], 'none');
            }

            vjezba1.click();
            for (let i = 1; i < pomocniDivVjezba1.childElementCount; i++) {
                assert.equal(pomocniDivVjezba1.children[i].style['display'], 'inline');
            }
            for (let i = 1; i < pomocniDivVjezba2.childElementCount; i++) {
                assert.equal(pomocniDivVjezba2.children[i].style['display'], 'none');
            }
            pomocniDiv.innerHTML = '';
        });

        it('Metoda se poziva više puta sa različitim brojem zadataka za jednu te istu vjezbu', function() {
            var pomocniDiv = document.getElementById('pomocniDiv');
            pomocniDiv.innerHTML = '';

            var vjezba1 = document.createElement('button');
            pomocniDiv.appendChild(vjezba1);
            VjezbeAjax.iscrtajZadatke(pomocniDiv, 2);
            assert.equal(pomocniDiv.childElementCount, 3);

            VjezbeAjax.iscrtajZadatke(pomocniDiv, 5);
            assert.equal(pomocniDiv.childElementCount, 6);

            VjezbeAjax.iscrtajZadatke(pomocniDiv, 1);
            assert.equal(pomocniDiv.childElementCount, 2);
            pomocniDiv.innerHTML = '';
        });


    });

    chai.should();
    describe('dohvatiPodatke()', function() {
        beforeEach(function() {
            this.xhr = sinon.useFakeXMLHttpRequest();

            this.requests = [];
            this.xhr.onCreate = function(xhr) {
                this.requests.push(xhr);
            }.bind(this);
        });

        afterEach(function() {
            this.xhr.restore();
        });

        it('Test ispravnih podataka', function(done) {
            VjezbeAjax.dohvatiPodatke(function(err, data) {
                assert.equal(err, null, 'Podaci su ispravni!');
                assert.deepEqual(data, { "brojVjezbi": 2, "brojZadataka": [1, 2] });
                done();
            });
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, '{"brojVjezbi":2, "brojZadataka":[1,2]}');
        });

        it('Test neispravnih podataka 1', function(done) {
            VjezbeAjax.dohvatiPodatke(function(err, data) {
                assert.equal(data, null, 'Podaci su neispravni!');
                assert.equal(err, "Pogrešan parametar brojVjezbi,brojZadataka");
                done();
            });
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, '{"brojVjezbi":-2, "brojZadataka":[1,2]}');
        });

        it('Test neispravnih podataka 2', function(done) {
            VjezbeAjax.dohvatiPodatke(function(err, data) {
                assert.equal(data, null, 'Podaci su neispravni!');
                assert.equal(err, "Pogrešan parametar z0,z1,z2,z3");
                done();
            });
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, '{"brojVjezbi":4, "brojZadataka":[-1,16,2.5,"nije broj"]}');
        });

        it('Test neispravnih podataka 3', function(done) {
            VjezbeAjax.dohvatiPodatke(function(err, data) {
                assert.equal(data, null, 'Podaci su neispravni!');
                assert.equal(err, "Pogrešan parametar brojVjezbi");
                done();
            });
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, '{"brojVjezbi":16, "brojZadataka":[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]}');
        });
        it('Test neispravnih podataka 4', function(done) {
            VjezbeAjax.dohvatiPodatke(function(err, data) {
                assert.equal(data, null, 'Podaci su neispravni!');
                assert.equal(err, "Pogrešan parametar brojVjezbi,brojZadataka");
                done();
            });
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, '{}');
        });
    });


    describe('posaljiPodatke()', function() {
        beforeEach(function() {
            this.xhr = sinon.useFakeXMLHttpRequest();

            this.requests = [];
            this.xhr.onCreate = function(xhr) {
                this.requests.push(xhr);
            }.bind(this);
        });

        afterEach(function() {
            this.xhr.restore();
        });

        it('Slanje validnih podataka', function(done) {
            var data1 = { "brojVjezbi": 2, "brojZadataka": [1, 2] };
            var dataJson = JSON.stringify(data1);
            VjezbeAjax.posaljiPodatke(data1, function(err, data) {
                assert.equal(err, null, 'Podaci su ispravni!');
                assert.deepEqual(data, { "brojVjezbi": 2, "brojZadataka": [1, 2] });
                done();
            });
            this.requests[0].requestBody.should.equal(dataJson);
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, dataJson);
        });
        it('Test neispravnih podataka 1', function(done) {
            var data1 = { "brojVjezbi": -2, "brojZadataka": [1, 2] };
            var dataJson = JSON.stringify(data1);
            VjezbeAjax.posaljiPodatke(data1, function(err, data) {
                assert.equal(data, null, 'Podaci su neispravni!');
                assert.equal(err, "Pogrešan parametar brojVjezbi,brojZadataka");
                done();
            });
            this.requests[0].requestBody.should.equal(dataJson);
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, '{"status":"error", "data":"Pogrešan parametar brojVjezbi,brojZadataka"}');
        });

        it('Test neispravnih podataka 2', function(done) {
            var data1 = { "brojVjezbi": 4, "brojZadataka": [-1, 16, 2.5, "nije broj"] }
            var dataJson = JSON.stringify(data1);
            VjezbeAjax.posaljiPodatke(data1, function(err, data) {
                assert.equal(data, null, 'Podaci su neispravni!');
                assert.equal(err, "Pogrešan parametar z0,z1,z2,z3");
                done();
            });
            this.requests[0].requestBody.should.equal(dataJson);
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, '{"status":"error", "data":"Pogrešan parametar z0,z1,z2,z3"}');
        });

        it('Test neispravnih podataka 3', function(done) {
            var data1 = { "brojVjezbi": 16, "brojZadataka": [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1] }
            var dataJson = JSON.stringify(data1);
            VjezbeAjax.posaljiPodatke(data1, function(err, data) {
                assert.equal(data, null, 'Podaci su neispravni!');
                assert.equal(err, "Pogrešan parametar brojVjezbi");
                done();
            });
            this.requests[0].requestBody.should.equal(dataJson);
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, '{"status":"error", "data":"Pogrešan parametar brojVjezbi"}');
        });
        it('Test neispravnih podataka 4', function(done) {
            var data1 = {}
            var dataJson = JSON.stringify(data1);
            VjezbeAjax.posaljiPodatke(data1, function(err, data) {
                assert.equal(data, null, 'Podaci su neispravni!');
                assert.equal(err, "Pogrešan parametar brojVjezbi,brojZadataka");
                done();
            });
            this.requests[0].requestBody.should.equal(dataJson);
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, '{"status":"error", "data":"Pogrešan parametar brojVjezbi,brojZadataka"}');
        });

        it('Test neispravnih podataka 5', function(done) {
            var data1 = { "brojVjezbi": 5 };
            var dataJson = JSON.stringify(data1);
            VjezbeAjax.posaljiPodatke(data1, function(err, data) {
                assert.equal(data, null, 'Podaci su neispravni!');
                assert.equal(err, "Pogrešan parametar brojZadataka");
                done();
            });
            this.requests[0].requestBody.should.equal(dataJson);
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, '{"status":"error", "data":"Pogrešan parametar brojZadataka"}');
        });

        it('Test neispravnih podataka 6', function(done) {
            var data1 = "Ovo nije JSON";

            VjezbeAjax.posaljiPodatke(data1, function(err, data) {
                assert.equal(data, null, 'Podaci su neispravni!');
                assert.equal(err, "Nije ispravan JSON!");
                done();
            });
            this.requests[0].requestBody.should.equal(JSON.stringify("Ovo nije JSON"));
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, 'Ovo nije JSON');
        });




    });
});