let assert = chai.assert;
describe('TestoviParser', function() {
    describe('PorediRezultate()', function() {
        it('Identicni testovi 1', function() {
            var rezultat1 = `{
                "stats":{
                    "suites":2,
                    "tests":2,
                    "passes":2,
                    "pending":0,
                    "failures":0,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"T1",
                        "fullTitle":"Test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[],
                "passes":[
                    {
                        "title":"T1",
                        "fullTitle":"Test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ]
            }`;

            var rezultat2 = `{
                "stats":{
                    "suites":2,
                    "tests":2,
                    "passes":1,
                    "pending":0,
                    "failures":1,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"T1",
                        "fullTitle":"Test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
               ],
                "passes":[
                    {
                        "title":"T1",
                        "fullTitle":"Test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ]
            }`;

            var obj = TestoviParser.porediRezultate(rezultat1, rezultat2);
            assert.equal(obj.promjena, "50%", "Promjena treba biti 50%");
            assert.equal(obj.greske.length, 1, "Postoji jedna greska");
            assert.deepEqual(obj.greske, ["Test 2"]);
        });

        it('Identicni testovi 2 - provjera leksikografskog poretka', function() {
            var rezultat1 = `{
                "stats":{
                    "suites":3,
                    "tests":3,
                    "passes":2,
                    "pending":0,
                    "failures":1,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"T1",
                        "fullTitle":"test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "passes":[
                    {
                        "title":"T1",
                        "fullTitle":"test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ]
            }`;

            var rezultat2 = `{
                "stats":{
                    "suites":3,
                    "tests":3,
                    "passes":1,
                    "pending":0,
                    "failures":2,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"T1",
                        "fullTitle":"test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[
                    {
                        "title":"T1",
                        "fullTitle":"test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "passes":[
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ]
            }`;

            var obj = TestoviParser.porediRezultate(rezultat1, rezultat2);
            assert.equal(obj.promjena, "33.3%", "Promjena treba biti 33.3% jer je to tacnost rezultata2");
            assert.equal(obj.greske.length, 2, "Postoje dvije greske nakon drugog testiranja");
            assert.deepEqual(obj.greske, ["Test 2", "test 1"], "'Test 2' je ispred 'test 1' po leksikografskom poretku!");
        });

        it('U funkciju poslan nevalidan JSON string - Testovi se ne mogu izvrsiti', function() {
            var rezultat1 = "Ovo nece raditi jer ovo nije JSON string";
            var rezultat2 = `{
                "stats":{
                    "suites":3,
                    "tests":3,
                    "passes":1,
                    "pending":0,
                    "failures":2,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"T1",
                        "fullTitle":"test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[
                    {
                        "title":"T1",
                        "fullTitle":"test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "passes":[
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ]
            }`;
            var test = TestoviParser.porediRezultate(rezultat1, rezultat2);
            assert.equal(JSON.stringify(test), '{"promjena":"0%","greske":["Testovi se ne mogu izvršiti"]}');
        });

        it('U funkciju poslan nevalidan JSON string - ne sadrzi sve potrebne atribute - Testovi se ne mogu izvrsiti', function() {
            var rezultat1 = `{
                "stats":{
                    "suites":3,
                    "tests":3,
                    "passes":1,
                    "pending":0,
                    "failures":2,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"T1",
                        "fullTitle":"test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[
                    {
                        "title":"T1",
                        "fullTitle":"test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "passes":[
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ]
            }`;
            var rezultat2 = `{
                "stats":{
                    "suites":1,
                    "tests":1,
                    "passes":1,
                    "pending":0,
                    "failures":0,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "pending":[],
                "passes":[]
            }`;
            var test = TestoviParser.porediRezultate(rezultat1, rezultat2);
            assert.equal(JSON.stringify(test), '{"promjena":"0%","greske":["Testovi se ne mogu izvršiti"]}');
        });

        it('Testovi nisu identicni - nema nijedan zajednicki test', function() {
            var rezultat1 = `{
                "stats":{
                    "suites":2,
                    "tests":2,
                    "passes":2,
                    "pending":0,
                    "failures":0,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"T1",
                        "fullTitle":"Test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[],
                "passes":[
                    {
                        "title":"T1",
                        "fullTitle":"Test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ]
            }`;

            var rezultat2 = `{
                "stats":{
                    "suites":2,
                    "tests":2,
                    "passes":2,
                    "pending":0,
                    "failures":0,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T4",
                        "fullTitle":"Test 4",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[],
                "passes":[
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T4",
                        "fullTitle":"Test 4",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ]
            }`;

            var obj = TestoviParser.porediRezultate(rezultat1, rezultat2);
            assert.equal(obj.promjena, "0%", "Promjena treba biti 0% posto svi testovi prolaze");
            assert.equal(obj.greske.length, 0, "Greske ne postoje");
            assert.deepEqual(obj.greske, []);
        });
        it('Testovi nisu identicni - postoji jedan zajednicki test koji prolazi u oba rezultata', function() {
            var rezultat1 = `{
                "stats":{
                    "suites":2,
                    "tests":2,
                    "passes":2,
                    "pending":0,
                    "failures":0,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"T1",
                        "fullTitle":"Test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[],
                "passes":[
                    {
                        "title":"T1",
                        "fullTitle":"Test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ]
            }`;

            var rezultat2 = `{
                "stats":{
                    "suites":2,
                    "tests":2,
                    "passes":1,
                    "pending":0,
                    "failures":1,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "passes":[
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ]
            }`;

            var obj = TestoviParser.porediRezultate(rezultat1, rezultat2);
            assert.equal(obj.promjena, "50%", "Promjena treba biti 50% posto svi pada jedan test od dva, buduci da ne uzimamo u razmatranje test iz rezultata jedan koji prolazi");
            assert.equal(obj.greske.length, 1, "Postoji jedna greska");
            assert.deepEqual(obj.greske, ["Test 3"]);
        });

        it('Testovi nisu identicni - postoji jedan zajednicki test koji prolazi u prvom rezultatu, a u drugom pada - u konacnici smatramo da taj test pada', function() {
            var rezultat1 = `{
                "stats":{
                    "suites":2,
                    "tests":2,
                    "passes":2,
                    "pending":0,
                    "failures":0,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"T1",
                        "fullTitle":"Test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[],
                "passes":[
                    {
                        "title":"T1",
                        "fullTitle":"Test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ]
            }`;

            var rezultat2 = `{
                "stats":{
                    "suites":2,
                    "tests":2,
                    "passes":0,
                    "pending":0,
                    "failures":2,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "passes":[]
            }`;

            var obj = TestoviParser.porediRezultate(rezultat1, rezultat2);
            assert.equal(obj.promjena, "100%", "Promjena treba biti 100% posto imamo ukupno dva testa koja padaju, i oba su iz rezultata 2, a rezultat2 ima sveukupno dva testa");
            assert.equal(obj.greske.length, 2, "Postoje dvije greske");
            assert.deepEqual(obj.greske, ["Test 2", "Test 3"]);
        });


        it('Testovi nisu identicni - postoji jedan zajednicki test koji pada u prvom rezultatu, a u drugom prolazi - u konacnici smatramo da taj test prolazi', function() {
            var rezultat1 = `{
                "stats":{
                    "suites":2,
                    "tests":2,
                    "passes":1,
                    "pending":0,
                    "failures":1,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"T1",
                        "fullTitle":"Test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "passes":[
                    {
                        "title":"T1",
                        "fullTitle":"Test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ]
            }`;

            var rezultat2 = `{
                "stats":{
                    "suites":2,
                    "tests":2,
                    "passes":2,
                    "pending":0,
                    "failures":0,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[],
                "passes":[
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ]
            }`;

            var obj = TestoviParser.porediRezultate(rezultat1, rezultat2);
            assert.equal(obj.promjena, "0%", "Promjena treba biti 0% posto nema testova koji padaju u rezultatu 2, a test koji pada u rezultatu1 smo popravili u rezultatu2");
            assert.equal(obj.greske.length, 0, "Nema gresaka");
            assert.deepEqual(obj.greske, []);
        });

        it('Testovi nisu identicni - nema zajednickog testa - provjera leksikografskog poretka gresaka', function() {
            var rezultat1 = `{
                "stats":{
                    "suites":2,
                    "tests":2,
                    "passes":1,
                    "pending":0,
                    "failures":1,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"T1",
                        "fullTitle":"Test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "passes":[
                    {
                        "title":"t1",
                        "fullTitle":"test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ]
            }`;

            var rezultat2 = `{
                "stats":{
                    "suites":2,
                    "tests":2,
                    "passes":1,
                    "pending":0,
                    "failures":1,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T0",
                        "fullTitle":"test 0",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[
                    {
                        "title":"T0",
                        "fullTitle":"Test 0",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "passes":[
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ]
            }`;

            var obj = TestoviParser.porediRezultate(rezultat1, rezultat2);
            assert.equal(obj.promjena, "66.7%", "Promjena treba biti 66.7% posto pada jedan test u rezultatu1 kojeg nema u rezultatu2, te pada jedan od dva testa u rezultatu2");
            assert.equal(obj.greske.length, 2, "Postoje dvije greske");
            assert.deepEqual(obj.greske, ["Test 2", "Test 0"], "Prvo moraju biti poredane greske iz rezultata jedan, a onda greske iz rezultata dva");
        });

        it('Testovi nisu identicni - ima vise zajednickih testova - provjera leksikografskog poretka gresaka ', function() {
            var rezultat1 = `{
                "stats":{
                    "suites":2,
                    "tests":3,
                    "passes":1,
                    "pending":0,
                    "failures":2,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"T1",
                        "fullTitle":"Test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "passes":[
                    {
                        "title":"t1",
                        "fullTitle":"test 1",
                        "file":null,
                        "duration":1,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ]
            }`;

            var rezultat2 = `{
                "stats":{
                    "suites":2,
                    "tests":4,
                    "passes":1,
                    "pending":0,
                    "failures":3,
                    "start":"2021-11-05T15:00:26.343Z",
                    "end":"2021-11-05T15:00:26.352Z",
                    "duration":9
                },
                "tests":[
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T5",
                        "fullTitle":"test 5",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T6",
                        "fullTitle":"Test 6",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "pending":[],
                "failures":[
                   
                    {
                        "title":"T5",
                        "fullTitle":"test 5",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T2",
                        "fullTitle":"Test 2",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    },
                    {
                        "title":"T6",
                        "fullTitle":"Test 6",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ],
                "passes":[
                    {
                        "title":"T3",
                        "fullTitle":"Test 3",
                        "file":null,
                        "duration":0,
                        "currentRetry":0,
                        "speed":"fast",
                        "err":{}
                    }
                ]
            }`;

            var obj = TestoviParser.porediRezultate(rezultat1, rezultat2);
            assert.equal(obj.promjena, "75%", "Promjena treba biti 75% posto svi testovi koji padaju u rezultatu1 se nalaze u rezultatu2, a u rezultatu2 imamo 3/4 testova koji padaju");
            assert.equal(obj.greske.length, 3, "Postoje tri greske");
            assert.deepEqual(obj.greske, ["Test 2", "Test 6", "test 5"]);
        });



    });
});