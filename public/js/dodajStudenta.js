document.getElementById("btnPosalji").addEventListener('click', function(event) {
    var objekat = {};
    document.getElementById("ajaxstatus").innerHTML = '';
    objekat['ime'] = document.getElementById('ime').value;
    objekat['prezime'] = document.getElementById('prezime').value;
    objekat['index'] = document.getElementById('index').value;
    objekat['grupa'] = document.getElementById('grupa').value;
    var greska = false;
    if (objekat['ime'] === '' || objekat['ime'] == null || objekat['ime'] == undefined) {
        console.log("Morate unijeti ime studenta!");
        greska = true;
    }
    if (objekat['prezime'] == '' || objekat['prezime'] == null || objekat['prezime'] == undefined) {
        console.log("Morate unijeti prezime studenta!");
        greska = true;
    }
    if (objekat['index'] == '' || objekat['index'] == null || objekat["index"] == undefined) {
        console.log("Morate unijeti index studenta!");
        greska = true;
    }
    if (objekat['grupa'] == '' || objekat['grupa'] == null || objekat['grupa'] == undefined) {
        console.log("Morate unijeti grupu studenta!");
        greska = true;
    }
    if (greska) {
        document.getElementById("ajaxstatus").innerHTML = "Niste unijeli sve podatke!";
    }
    if (!greska) {
        StudentAjax.dodajStudenta(objekat, (err, data) => {
            if (data == null)
                document.getElementById("ajaxstatus").innerHTML = err;
            else {
                document.getElementById("ajaxstatus").innerHTML = data.status;
            }
        });
    }

});