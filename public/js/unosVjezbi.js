document.getElementById("id").addEventListener('change', function(event) {
    VjezbeAjax.dodajInputPolja(document.getElementById("zadaci"), parseFloat(document.getElementById("id").value));
});

document.getElementById("btnPosalji").addEventListener('click', function(event) {
    var objekat = {};
    objekat['brojVjezbi'] = parseFloat(document.getElementById('id').value);
    if (isNaN(objekat['brojVjezbi']))
        objekat['brojVjezbi'] = 0;
    objekat['brojZadataka'] = [];
    for (let i = 0; i < document.getElementById('zadaci').childElementCount / 2; i++) {
        var id = 'z' + i;
        objekat['brojZadataka'][i] = parseFloat(document.getElementById(id).value);
    }
    VjezbeAjax.posaljiPodatke(objekat, (err, data) => {
        if (data == null)
            console.log(err);
        else {
            console.log("Uspjesno uneseni podaci: " + JSON.stringify(data));
        }
    });
});