const Sequelize = require('sequelize');
const sequelize = require('../baza.js');
const chaihttp = require('chai-http');
const chai = require('chai');

chai.use(chaihttp);
let assert = chai.assert;
chai.should();

var Student = require("../models/Student.js")(sequelize);
var Vjezba = require("../models/Vjezba.js")(sequelize);
var Grupa = require("../models/Grupa.js")(sequelize);
var Zadatak = require("../models/Zadatak.js")(sequelize);

Vjezba.hasMany(Zadatak, { as: "zadaciVjezbe" });
Grupa.hasMany(Student, { as: "studentiGrupe" });

const server = require('../index.js');

describe('Testovi ruta', function() {
    var studenti = [];
    var vjezbe = [];
    var grupe = [];
    var zadaci = [];
    this.timeout(10000);
    this.beforeAll(function(done) {
        sequelize.sync().then(() => {
            Student.findAll().then(s => {
                s.forEach(stud => {
                    studenti.push({ id: stud.id, ime: stud.ime, prezime: stud.prezime, index: stud.index, GrupaId: stud.GrupaId });
                })
                return Grupa.findAll();
            }).then(g => {
                g.forEach(gr => {
                    grupe.push({ id: gr.id, naziv: gr.naziv });
                })
                return Vjezba.findAll();
            }).then(v => {
                v.forEach(vj => {
                    vjezbe.push({ id: v.id, naziv: vj.naziv });
                })
                return Zadatak.findAll();
            }).then(z => {
                z.forEach(zad => {
                    zadaci.push({ id: zad.id, naziv: zad.naziv, VjezbaId: zad.VjezbaId });
                })
                sequelize.sync({ force: true }).then(function() {
                    done();
                })
            })
        })
    })

    this.afterAll(function(done) {
        sequelize.sync({ force: true }).then(() => {
            Grupa.bulkCreate(grupe).then(() => {
                return Student.bulkCreate(studenti);

            }).then(() => {
                return Vjezba.bulkCreate(vjezbe);
            }).then(() => {
                return Zadatak.bulkCreate(zadaci);
            }).then(() => {
                done();
            })
        })
    })

    this.beforeEach(function(done) {
        sequelize.sync({ force: true }).then(() => { done() });
    })
    this.afterEach(function(done) {
        sequelize.sync({ force: true }).then(() => { done() });
    })

    describe('POST/student', function() {
        it("Dodavanje jednog studenta", async function() {
            var student = {
                "ime": "Samra",
                "prezime": "Behic",
                "index": "18602",
                "grupa": "RI3-1"
            }
            var res = await chai.request(server)
                .post('/student')
                .set('content-type', 'application/json')
                .send(JSON.stringify(student));
            var studenti = await Student.findAll();
            assert.equal(studenti.length, 1, "Dodan je samo jedan student!");
            var s = studenti[0];
            assert.equal(s.ime, student.ime);
            assert.equal(s.prezime, student.prezime);
            assert.equal(s.index, student.index);
            var grupe = await Grupa.findAll();
            assert.equal(grupe.length, 1, "Dodana je samo jedna grupa!");
            var g = grupe[0];
            assert.equal(s.GrupaId, g.id);
        });

        it("Dodavanje jednog studenta - provjera odgovora servera", async function() {
            var student = {
                "ime": "Samra",
                "prezime": "Behic",
                "index": "18602",
                "grupa": "RI3-1"
            }
            var res = await chai.request(server)
                .post('/student')
                .set('content-type', 'application/json')
                .send(JSON.stringify(student));
            assert.equal(JSON.parse(res.text).status, "Kreiran student!", "Odgovor mora biti u odgovarajućem formatu!");
            var studenti = await Student.findAll();
            assert.equal(studenti.length, 1, "Dodan je samo jedan student!");
            var s = studenti[0];
            assert.equal(s.ime, student.ime);
            assert.equal(s.prezime, student.prezime);
            assert.equal(s.index, student.index);
            var grupe = await Grupa.findAll();
            assert.equal(grupe.length, 1, "Dodana je samo jedna grupa!");
            var g = grupe[0];
            assert.equal(s.GrupaId, g.id);
        });

        it("Dodavanje više studenata zaredom sa istim grupama", async function() {
            var nizStudenata = [];
            var s1 = {
                "ime": "Samra",
                "prezime": "Behic",
                "index": "18602",
                "grupa": "RI3-1"
            }
            var s2 = {
                "ime": "Mujo",
                "prezime": "Mujic",
                "index": "11111",
                "grupa": "RI3-1"
            }
            var s3 = {
                "ime": "Hana",
                "prezime": "Hanic",
                "index": "22222",
                "grupa": "RI3-1"
            }
            nizStudenata.push(s1);
            nizStudenata.push(s2);
            nizStudenata.push(s3);
            await chai.request(server)
                .post('/student')
                .set('content-type', 'application/json')
                .send(JSON.stringify(s1));
            await chai.request(server)
                .post('/student')
                .set('content-type', 'application/json')
                .send(JSON.stringify(s2));
            await chai.request(server)
                .post('/student')
                .set('content-type', 'application/json')
                .send(JSON.stringify(s3));
            var studenti = await Student.findAll();
            assert.equal(studenti.length, 3, "Dodana su tri studenta student!");
            var grupe = await Grupa.findAll();
            assert.equal(grupe.length, 1, "Dodana je samo jedna grupa!");
            var g = grupe[0];
            for (let i = 0; i < 3; i++) {
                let s = await Student.findAll({ where: { index: nizStudenata[i].index } });
                assert.equal(s.length, 1);
                assert.equal(s[0].ime, nizStudenata[i].ime);
                assert.equal(s[0].prezime, nizStudenata[i].prezime);
                assert.equal(s[0].index, nizStudenata[i].index);
                assert.equal(s[0].GrupaId, g.id);
            }
        });

        it("Dodavanje više studenata zaredom sa različitim grupama grupama", async function() {
            var nizStudenata = [];
            var s1 = {
                "ime": "Samra",
                "prezime": "Behic",
                "index": "18602",
                "grupa": "RI3-1"
            }
            var s2 = {
                "ime": "Mujo",
                "prezime": "Mujic",
                "index": "11111",
                "grupa": "RI3-2"
            }
            var s3 = {
                "ime": "Hana",
                "prezime": "Hanic",
                "index": "22222",
                "grupa": "RI3-1"
            }
            nizStudenata.push(s1);
            nizStudenata.push(s2);
            nizStudenata.push(s3);
            await chai.request(server)
                .post('/student')
                .set('content-type', 'application/json')
                .send(JSON.stringify(s1));
            await chai.request(server)
                .post('/student')
                .set('content-type', 'application/json')
                .send(JSON.stringify(s2));
            await chai.request(server)
                .post('/student')
                .set('content-type', 'application/json')
                .send(JSON.stringify(s3));

            var studenti = await Student.findAll();
            assert.equal(studenti.length, 3, "Dodana su tri studenta student!");
            var grupe = await Grupa.findAll();
            assert.equal(grupe.length, 2, "Dodane su dvije grupe!");

            for (let i = 0; i < 3; i++) {
                let s = await Student.findAll({ where: { index: nizStudenata[i].index } });
                assert.equal(s.length, 1);
                assert.equal(s[0].ime, nizStudenata[i].ime);
                assert.equal(s[0].prezime, nizStudenata[i].prezime);
                assert.equal(s[0].index, nizStudenata[i].index);
                let g = await Grupa.findOne({ where: { id: s[0].GrupaId } })
                assert.equal(g.naziv, nizStudenata[i].grupa);
            }
        });

        it("Dodavanje studenta čiji indeks već postoji", async function() {
            var s1 = {
                "ime": "Samra",
                "prezime": "Behic",
                "index": "18602",
                "grupa": "RI3-1"
            }
            var s2 = {
                "ime": "Mujo",
                "prezime": "Mujic",
                "index": "18602",
                "grupa": "RI3-1"
            }
            await chai.request(server)
                .post('/student')
                .set('content-type', 'application/json')
                .send(JSON.stringify(s1));
            var res = await chai.request(server)
                .post('/student')
                .set('content-type', 'application/json')
                .send(JSON.stringify(s2));

            var studenti = await Student.findAll();
            assert.equal(studenti.length, 1, "Dodan je samo prvi student!");
            var s = studenti[0];
            assert.equal(s.ime, s1.ime);
            assert.equal(s.prezime, s1.prezime);
            assert.equal(s.index, s1.index);
            assert.equal(JSON.parse(res.text).status, "Student sa indexom 18602 već postoji!", "Odgovor mora biti u odgovarajućem formatu!");
        })

        it("Dodavanje studenta čiji indeks već postoji, provjera dodanih grupa", async function() {
            var s1 = {
                "ime": "Samra",
                "prezime": "Behic",
                "index": "18602",
                "grupa": "RI3-1"
            }
            var s2 = {
                "ime": "Mujo",
                "prezime": "Mujic",
                "index": "18602",
                "grupa": "RI3-2"
            }
            await chai.request(server)
                .post('/student')
                .set('content-type', 'application/json')
                .send(JSON.stringify(s1));
            var res = await chai.request(server)
                .post('/student')
                .set('content-type', 'application/json')
                .send(JSON.stringify(s2));

            var studenti = await Student.findAll();
            assert.equal(studenti.length, 1, "Dodan je samo prvi student!");
            assert.equal(JSON.parse(res.text).status, "Student sa indexom 18602 već postoji!", "Odgovor mora biti u odgovarajućem formatu!");
            var grupe = await Grupa.findAll();
            assert.equal(grupe.length, 1, "Dodana je grupa samo za prvog studenta!");
            var g1 = await Grupa.findAll({ where: { naziv: s1.grupa } });
            assert.equal(g1.length, 1, "Dodana je grupa za prvog studenta!");
            var g2 = await Grupa.findAll({ where: { naziv: s2.grupa } });
            assert.equal(g2.length, 0, "Grupa za drugog studenta ne treba biti dodana!");
        })


        it("Dodavanje više studenata sa istim grupama i istim studentima", async function() {
            var nizStudenata = [];
            var s1 = {
                "ime": "Samra",
                "prezime": "Behic",
                "index": "18602",
                "grupa": "RI3-1"
            }
            var s2 = {
                "ime": "Mujo",
                "prezime": "Mujic",
                "index": "18602",
                "grupa": "RI3-2"
            }
            var s3 = {
                "ime": "Hana",
                "prezime": "Hanic",
                "index": "22222",
                "grupa": "RI3-1"
            }
            var s4 = {
                "ime": "Elmo",
                "prezime": "Elmic",
                "index": "333333",
                "grupa": "RI3-3"
            }
            var s5 = {
                "ime": "Elmo",
                "prezime": "Elmic",
                "index": "333333",
                "grupa": "RI3-1"
            }
            nizStudenata.push(s1);
            nizStudenata.push(s2);
            nizStudenata.push(s3);
            nizStudenata.push(s4);
            nizStudenata.push(s5);
            nizStudenataDodanih = [];
            nizStudenataDodanih.push(s1);
            nizStudenataDodanih.push(s3);
            nizStudenataDodanih.push(s4);
            var odgovori = [];
            for (let i = 0; i < 5; i++) {
                let res = await chai.request(server)
                    .post('/student')
                    .set('content-type', 'application/json')
                    .send(JSON.stringify(nizStudenata[i]));
                odgovori.push(res);
            }
            assert.equal(JSON.parse(odgovori[0].text).status, "Kreiran student!", "Odgovor mora biti u odgovarajućem formatu!");
            assert.equal(JSON.parse(odgovori[1].text).status, "Student sa indexom 18602 već postoji!", "Odgovor mora biti u odgovarajućem formatu!");
            assert.equal(JSON.parse(odgovori[2].text).status, "Kreiran student!", "Odgovor mora biti u odgovarajućem formatu!");
            assert.equal(JSON.parse(odgovori[3].text).status, "Kreiran student!", "Odgovor mora biti u odgovarajućem formatu!");
            assert.equal(JSON.parse(odgovori[4].text).status, "Student sa indexom 333333 već postoji!", "Odgovor mora biti u odgovarajućem formatu!");


            var studenti = await Student.findAll();
            assert.equal(studenti.length, 3, "Dodana su tri studenta student!");
            var grupe = await Grupa.findAll();
            assert.equal(grupe.length, 2, "Dodane su dvije grupe!");

            for (let i = 0; i < 3; i++) {
                let s = await Student.findAll({ where: { index: nizStudenataDodanih[i].index } });
                assert.equal(s.length, 1);
                assert.equal(s[0].ime, nizStudenataDodanih[i].ime);
                assert.equal(s[0].prezime, nizStudenataDodanih[i].prezime);
                assert.equal(s[0].index, nizStudenataDodanih[i].index);
                let g = await Grupa.findOne({ where: { id: s[0].GrupaId } })
                assert.equal(g.naziv, nizStudenataDodanih[i].grupa);
            }
        });

        it("Pokušaj dodavanja studenta bez svih podataka - ne smije krahirati", async function() {
            var s1 = {}
            var s2 = {
                "ime": "Samra",
                "prezime": "Behic"
            }
            var s3 = {
                "index": "18602"
            }
            var nizStudenata = [];
            nizStudenata.push(s1);
            nizStudenata.push(s2);
            nizStudenata.push(s3);

            for (let i = 0; i < 3; i++) {
                await chai.request(server)
                    .post('/student')
                    .set('content-type', 'application/json')
                    .send(JSON.stringify(nizStudenata[i]));
            }
            var studenti = await Student.findAll();
            assert.equal(studenti.length, 0, "Dodano 0 studenata!");
        })

    })

    describe('PUT/student/:index', function() {
        it("Promjena grupe studenta koji postoji na grupu koja postoji", async function() {
            var gr1 = await Grupa.create({ naziv: "grupa1" });
            await Grupa.create({ naziv: "grupa2" });
            var grupaNova = { grupa: "grupa2" };
            await gr1.createStudentiGrupe({ ime: "Samra", prezime: "Behic", index: "18602" });
            var res = await chai.request(server)
                .put('/student/18602')
                .set('content-type', 'application/json')
                .send(JSON.stringify(grupaNova));
            assert.equal(JSON.parse(res.text).status, "Promjenjena grupa studentu 18602", "Odgovor mora biti u odgovarajućem formatu!");
            var grupe = await Grupa.findAll();
            assert.equal(grupe.length, 2, "Obje grupe su već postojale, ne treba ništa dodavati!");
        });
        it("Promjena grupe studenta koji postoji na grupu koja ne postoji", async function() {
            var gr1 = await Grupa.create({ naziv: "grupa1" });
            var grupaNova = { grupa: "grupa2" };
            await gr1.createStudentiGrupe({ ime: "Samra", prezime: "Behic", index: "18602" });
            var res = await chai.request(server)
                .put('/student/18602')
                .set('content-type', 'application/json')
                .send(JSON.stringify(grupaNova));
            assert.equal(JSON.parse(res.text).status, "Promjenjena grupa studentu 18602", "Odgovor mora biti u odgovarajućem formatu!");
            var grupe = await Grupa.findAll();
            assert.equal(grupe.length, 2, "Grupa koja nije postojala je dodana!");
        });
        it("Promjena grupe studenta koji ne postoji na grupu koja postoji", async function() {
            var grupaNova = { grupa: "grupa2" };
            var res = await chai.request(server)
                .put('/student/18602')
                .set('content-type', 'application/json')
                .send(JSON.stringify(grupaNova));
            assert.equal(JSON.parse(res.text).status, "Student sa indexom 18602 ne postoji", "Odgovor mora biti u odgovarajućem formatu!");
        });
        it("Promjena grupe studenta koji ne postoji na grupu koja ne postoji", async function() {
            var gr1 = await Grupa.create({ naziv: "grupa1" });
            var grupaNova = { grupa: "grupa2" };
            var res = await chai.request(server)
                .put('/student/18602')
                .set('content-type', 'application/json')
                .send(JSON.stringify(grupaNova));
            assert.equal(JSON.parse(res.text).status, "Student sa indexom 18602 ne postoji", "Odgovor mora biti u odgovarajućem formatu!");
            var grupe = await Grupa.findAll();
            assert.equal(grupe.length, 1, "Grupa za studenta koji ne postoji ne treba biti dodana!");
        });

        it("Promjena grupe studenta pri cemu grupa nije poslana - ne smije krahirati i nece se nista desiti", async function() {
            var gr1 = await Grupa.create({ naziv: "grupa1" });
            var grupaNova = {};
            await gr1.createStudentiGrupe({ ime: "Samra", prezime: "Behic", index: "18602" });
            var res = await chai.request(server)
                .put('/student/18602')
                .set('content-type', 'application/json')
                .send(JSON.stringify(grupaNova));
            var grupe = await Grupa.findAll();
            assert.equal(grupe.length, 1, "Nista se ne smije promijeniti!");
            var s = await Student.findOne({ where: { index: "18602" } });
            var grupa = await Grupa.findOne({ where: { id: s.GrupaId } });
            assert.equal(grupa.naziv, gr1.naziv), "Nista se ne smije promijeniti!";
        });
    })

    describe('POST /batch/student', function() {
        it('Dodavanje više studenata u praznu tabelu sa različitim indeksima i istim grupama', async function() {
            var s1 = "Samra,Behic,18602,RI3-1";
            var s2 = "Mujo,Mujic,18603,RI3-1";
            var s3 = "Hana,Hanic,18604,RI3-1";
            var nizStudenata = [];
            nizStudenata.push(s1);
            nizStudenata.push(s2);
            nizStudenata.push(s3);
            var string = nizStudenata.join('\n');
            var res = await chai.request(server)
                .post('/batch/student')
                .set('content-type', 'text/plain')
                .send(string);
            assert.equal(JSON.parse(res.text).status, "Dodano 3 studenata!", "Odgovor mora biti u odgovarajućem formatu!");
            var studenti = await Student.findAll();
            assert.equal(studenti.length, 3, "Dodana su sva tri studenta!");
            var grupe = await Grupa.findAll();
            assert.equal(grupe.length, 1, "Dodana je samo jedna grupa!");
            var s11 = await Student.findOne({ where: { index: 18602 } });
            if (s11 == undefined)
                s11 = null
            assert.isNotNull(s11, "Student sa indexom 18602 treba biti dodan!");
            var s22 = await Student.findOne({ where: { index: 18603 } });
            if (s22 == undefined)
                s22 = null
            assert.isNotNull(s22, "Student sa indexom 18603 treba biti dodan!");
            var s33 = await Student.findOne({ where: { index: 18604 } });
            if (s33 == undefined)
                s33 = null
            assert.isNotNull(s33, "Student sa indexom 18604 treba biti dodan!");
            var gr = grupe[0];
            assert.equal(gr.naziv, "RI3-1");
            assert.equal(s11.GrupaId, gr.id, "Grupa prvog studenta je RI3-1");
            assert.equal(s22.GrupaId, gr.id, "Grupa drugog studenta je RI3-1");
            assert.equal(s33.GrupaId, gr.id, "Grupa treceg studenta je RI3-1");
        });

        it('Dodavanje više studenata u praznu tabelu sa različitim indeksima i različitim grupama', async function() {
            var s1 = "Samra,Behic,18602,RI3-1";
            var s2 = "Mujo,Mujic,18603,RI3-2";
            var s3 = "Hana,Hanic,18604,RI3-1";
            var nizStudenata = [];
            nizStudenata.push(s1);
            nizStudenata.push(s2);
            nizStudenata.push(s3);
            var string = nizStudenata.join('\n');
            var res = await chai.request(server)
                .post('/batch/student')
                .set('content-type', 'text/plain')
                .send(string);
            assert.equal(JSON.parse(res.text).status, "Dodano 3 studenata!", "Odgovor mora biti u odgovarajućem formatu!");
            var studenti = await Student.findAll();
            assert.equal(studenti.length, 3, "Dodana su sva tri studenta!");
            var grupe = await Grupa.findAll();
            assert.equal(grupe.length, 2, "Dodane su dvije grupe!");
            var s11 = await Student.findOne({ where: { index: 18602 } });
            if (s11 == undefined)
                s11 = null
            assert.isNotNull(s11, "Student sa indexom 18602 treba biti dodan!");
            var s22 = await Student.findOne({ where: { index: 18603 } });
            if (s22 == undefined)
                s22 = null
            assert.isNotNull(s22, "Student sa indexom 18603 treba biti dodan!");
            var s33 = await Student.findOne({ where: { index: 18604 } });
            if (s33 == undefined)
                s33 = null
            assert.isNotNull(s33, "Student sa indexom 18604 treba biti dodan!");
            var gr1 = await Grupa.findOne({ where: { naziv: "RI3-1" } });
            if (gr1 == undefined)
                gr1 = null;
            assert.isNotNull(gr1, "Mora biti dodana grupa RI3-1");
            var gr2 = await Grupa.findOne({ where: { naziv: "RI3-2" } });
            if (gr2 == undefined)
                gr2 = null;
            assert.isNotNull(gr2, "Mora biti dodana grupa RI3-1");
            assert.equal(s11.GrupaId, gr1.id, "Grupa prvog studenta je RI3-1");
            assert.equal(s22.GrupaId, gr2.id, "Grupa drugog studenta je RI3-2");
            assert.equal(s33.GrupaId, gr1.id, "Grupa treceg studenta je RI3-1");
        });
        it('Dodavanje više studenata sa grupama koje postoje pri čemu indeksi nekih studenata već postoje', async function() {
            var gr = await Grupa.create({ naziv: "grupa1" });
            await gr.createStudentiGrupe({ ime: "Samra", prezime: "Behic", index: "18602" });
            var s1 = "Samra,Behic,18602,RI3-1";
            var s2 = "Mujo,Mujic,18603,RI3-2";
            var s3 = "Hana,Hanic,18604,grupa1";
            var nizStudenata = [];
            nizStudenata.push(s1);
            nizStudenata.push(s2);
            nizStudenata.push(s3);
            var string = nizStudenata.join('\n');
            var res = await chai.request(server)
                .post('/batch/student')
                .set('content-type', 'text/plain')
                .send(string);
            assert.equal(JSON.parse(res.text).status, "Dodano 2 studenata, a studenti 18602 već postoje!", "Odgovor mora biti u odgovarajućem formatu!");
            var grupaNepostojeca = await Grupa.findOne({ where: { naziv: "RI3-1" } });
            if (grupaNepostojeca == undefined)
                grupaNepostojeca = null;
            assert.isNull(grupaNepostojeca, "Grupa RI3-1 ne treba biti dodana!");
            var s22 = await Student.findOne({ where: { index: 18603 } });
            if (s22 == undefined)
                s22 = null
            assert.isNotNull(s22, "Student sa indexom 18603 treba biti dodan!");
            var s33 = await Student.findOne({ where: { index: 18604 } });
            if (s33 == undefined)
                s33 = null
            assert.isNotNull(s33, "Student sa indexom 18604 treba biti dodan!");
            var grupe = await Grupa.findAll();
            assert.equal(grupe.length, 2, "U bazi trebaju biti dvije grupe!");

            var gr1 = await Grupa.findOne({ where: { naziv: "RI3-2" } });
            if (gr1 == undefined)
                gr1 = null;
            assert.isNotNull(gr1, "Mora biti dodana grupa RI3-2");
            var gr2 = await Grupa.findOne({ where: { naziv: "grupa1" } });
            if (gr2 == undefined)
                gr2 = null;
            assert.isNotNull(gr2, "Mora biti u bazi grupa1");
            assert.equal(s22.GrupaId, gr1.id, "Grupa prvog studenta je RI3-1");
            assert.equal(s33.GrupaId, gr2.id, "Grupa drugog studenta je grupa1");
        });

        it('Dodavanje više nevalidnih studenata, provjera redoslijeda nevalidnih index-a', async function() {
            var gr = await Grupa.create({ naziv: "grupa1" });
            await gr.createStudentiGrupe({ ime: "Samra", prezime: "Behic", index: "18602" });
            await gr.createStudentiGrupe({ ime: "Meho", prezime: "Mehic", index: "18603" });
            await gr.createStudentiGrupe({ ime: "Selmo", prezime: "Selmic", index: "18605" });

            var s1 = "Samra,Behic,18602,RI3-1";
            var s2 = "Mujo,Mujic,18605,RI3-2";
            var s3 = "Marko,Markic,18604,grupa1";
            var s4 = "Hana,Hanic,18603,grupa1";
            var nizStudenata = [];
            nizStudenata.push(s1);
            nizStudenata.push(s2);
            nizStudenata.push(s3);
            nizStudenata.push(s4);

            var string = nizStudenata.join('\n');
            var res = await chai.request(server)
                .post('/batch/student')
                .set('content-type', 'text/plain')
                .send(string);
            assert.equal(JSON.parse(res.text).status, "Dodano 1 studenata, a studenti 18602,18605,18603 već postoje!", "Odgovor mora biti u odgovarajućem formatu!");

            var grupaNepostojeca = await Grupa.findOne({ where: { naziv: "RI3-1" } });
            if (grupaNepostojeca == undefined)
                grupaNepostojeca = null;
            assert.isNull(grupaNepostojeca, "Grupa RI3-1 ne treba biti dodana!");
            var grupaNepostojeca2 = await Grupa.findOne({ where: { naziv: "RI3-2" } });
            if (grupaNepostojeca2 == undefined)
                grupaNepostojeca2 = null;
            assert.isNull(grupaNepostojeca2, "Grupa RI3-2 ne treba biti dodana!");

            var res2 = await chai.request(server)
                .post('/batch/student')
                .set('content-type', 'text/plain')
                .send(string);
            assert.equal(JSON.parse(res2.text).status, "Dodano 0 studenata, a studenti 18602,18605,18604,18603 već postoje!", "Odgovor mora biti u odgovarajućem formatu!");
        });
        it('Dodavanje više nevalidnih studenata, pri čemu u csv-u više studenata ima isti index', async function() {
            var gr = await Grupa.create({ naziv: "grupa1" });
            await gr.createStudentiGrupe({ ime: "Mina", prezime: "Minicic", index: "18602" });

            var s1 = "Samra,Behic,18602,RI3-1";
            var s2 = "Mujo,Mujic,18602,RI3-2";
            var s3 = "Marko,Markic,18604,grupa1";
            var s4 = "Niko,Nikic,18604,grupa1";
            var s5 = "Haso,Hasic,18604,grupa2";
            var s6 = "Hana,Hanic,18603,grupa1";
            var nizStudenata = [];
            nizStudenata.push(s1);
            nizStudenata.push(s2);
            nizStudenata.push(s3);
            nizStudenata.push(s4);
            nizStudenata.push(s5);
            nizStudenata.push(s6);

            var string = nizStudenata.join('\n');
            var res = await chai.request(server)
                .post('/batch/student')
                .set('content-type', 'text/plain')
                .send(string);
            assert.equal(JSON.parse(res.text).status, "Dodano 2 studenata, a studenti 18602,18602,18604,18604 već postoje!", "Odgovor mora biti u odgovarajućem formatu!");

            var studenti = await Student.findAll();
            assert.equal(studenti.length, 3, "Dodana su dva studenta, a jedan je vec postojao");

            var stud18604 = await Student.findAll({ where: { index: "18604" } });
            assert.equal(stud18604.length, 1);
            var s44 = stud18604[0];
            assert.equal(s44.ime, "Marko", "Mora biti dodan prvi student iz csv-a, ostali postaju nevalidni");
            assert.equal(s44.prezime, "Markic");
            assert.equal(s44.index, "18604");
            assert.equal(s44.GrupaId, gr.id);

            var grupe = await Grupa.findAll();
            assert.equal(grupe.length, 1, "Ne treba dodati nijednu novu grupu!");
        });
    })

})