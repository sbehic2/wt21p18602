const Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
    const Zadatak = sequelize.define("Zadatak", {
        naziv: { type: Sequelize.STRING({ binary: true }) }
    })
    return Zadatak;
};