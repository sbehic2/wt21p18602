const Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
    const Student = sequelize.define("Student", {
        ime: { type: Sequelize.STRING({ binary: true }) },
        prezime: { type: Sequelize.STRING({ binary: true }) },
        index: {
            type: Sequelize.STRING({ binary: true }),
            unique: true
        }
    })
    return Student;
};