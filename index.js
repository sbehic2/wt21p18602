const express = require('express');
var fs = require('fs');
const app = express();
const bodyParser = require('body-parser');
const sequelize = require('./baza.js');

app.use(express.static('./public'));
app.use(express.static('./public/html'));
app.use(express.static('./public/css'));
app.use(express.static('./public/images'));
app.use(express.static('./public/js'));


const Student = require('./models/Student.js')(sequelize);
const Grupa = require('./models/Grupa.js')(sequelize);
const Vjezba = require('./models/Vjezba.js')(sequelize);
const Zadatak = require('./models/Zadatak.js')(sequelize);

Grupa.hasMany(Student, { as: 'studentiGrupe' });
Vjezba.hasMany(Zadatak, { as: 'zadaciVjezbe' });

sequelize.sync();

app.use(bodyParser.json());
app.use(bodyParser.text());

app.get('/vjezbe/', function(req, res) {
    var obj = {};
    obj['brojVjezbi'] = 0;
    obj['brojZadataka'] = [];
    Vjezba.findAll().then(function(vjezbe) {
        Zadatak.findAll().then(function(zadaci) {
            for (let i = 0; i < vjezbe.length; i++) {
                obj['brojVjezbi']++;
                let brZad = 0;
                for (let j = 0; j < zadaci.length; j++) {
                    if (zadaci[j].VjezbaId == vjezbe[i].id)
                        brZad++;
                }
                obj['brojZadataka'].push(brZad);
            }
            res.json(obj);
        });
    })
});

app.post('/vjezbe', function(req, res) {
    let parametri = req.body;
    let porukaGreske = 'Pogrešan parametar ';
    let imaGreske = false;
    if (!Number.isInteger(parametri['brojVjezbi']) || parametri['brojVjezbi'] < 1 || parametri['brojVjezbi'] > 15) {
        imaGreske = true;
        porukaGreske += 'brojVjezbi';
    }
    if (parametri['brojZadataka'] != null) {
        for (let i = 0; i < parametri['brojZadataka'].length; i++) {
            if (!Number.isInteger(parametri['brojZadataka'][i]) || parametri['brojZadataka'][i] < 0 || parametri['brojZadataka'][i] > 10) {
                if (imaGreske)
                    porukaGreske += ',';
                imaGreske = true;
                porukaGreske += 'z' + i;
            }

        }
        if (parametri['brojZadataka'].length != parametri['brojVjezbi']) {
            if (imaGreske)
                porukaGreske += ',';
            porukaGreske += 'brojZadataka';
            imaGreske = true;
        }
    } else {
        if (imaGreske)
            porukaGreske += ',';
        porukaGreske += 'brojZadataka';
        imaGreske = true;
    }

    var greska = {};

    if (imaGreske) {
        greska['status'] = 'error';
        greska['data'] = porukaGreske;
        res.json(greska);
        return;
    }

    var nizVjezbi = [];
    for (let i = 1; i <= parametri['brojVjezbi']; i++) {
        let naziVjezbe = "Vježba " + i;
        let idVj = i;
        nizVjezbi.push({ id: idVj, naziv: naziVjezbe });
    }

    Zadatak.destroy({
        where: {},
        truncate: { cascade: true }
    }).then(() => {
        Vjezba.destroy({
            where: {},
            truncate: { cascade: true }
        }).then(() => {
            Vjezba.bulkCreate(nizVjezbi).then(() => {
                return Vjezba.findAll();
            }).then((vjezbe) => {
                var idZadatka = 1;
                var niz = [];
                vjezbe.forEach(vjezba => {
                    for (let i = 1; i <= parametri['brojZadataka'][vjezba.id - 1]; i++) {
                        var nazivZadatka = "Zadatak " + i;
                        niz.push(vjezba.createZadaciVjezbe({ id: idZadatka, naziv: nazivZadatka }));
                        idZadatka++;
                    }
                });
                Promise.all(niz).then(r => {
                    res.json(parametri);
                });
            })
        })
    })
});


app.post('/student', function(req, res) {
    let parametri = req.body;
    if (!parametri.hasOwnProperty('index')) {
        res.json({ "status": "Niste unijeli sve potrebne podatke!" });
        return;
    }
    let indexStudenta = parametri['index'];
    Student.findAll({ where: { index: indexStudenta } }).then(studenti => {
        if (studenti.length != 0) {
            let poruka = "Student sa indexom " + indexStudenta + " već postoji!";
            res.json({ "status": poruka });
            return;
        }
        if (!parametri.hasOwnProperty('ime') || !parametri.hasOwnProperty('prezime') || !parametri.hasOwnProperty('grupa')) {
            res.json({ "status": "Niste unijeli sve potrebne podatke!" });
            return;
        }
        Grupa.findAll({ where: { naziv: parametri['grupa'] } }).then(grupe => {
            if (grupe.length == 0) {
                return Grupa.count().then(x => {
                    return Grupa.create({ naziv: parametri['grupa'] });
                })

            } else
                return Grupa.findOne({ where: { naziv: parametri['grupa'] } })
        }).then(grupa => {
            grupa.createStudentiGrupe({ ime: parametri['ime'], prezime: parametri['prezime'], index: parametri['index'] }).then(() => {
                res.json({ "status": "Kreiran student!" });
            });
        })
    })
});

app.put('/student/:index', function(req, res) {
    let indexStudenta = req.params.index;
    let parametri = req.body;


    Student.findAll({ where: { index: indexStudenta } }).then(studenti => {
        if (studenti.length == 0) {
            let poruka = "Student sa indexom " + indexStudenta + " ne postoji";
            res.json({ "status": poruka });
            return;
        }
        if (!parametri.hasOwnProperty('grupa')) {
            res.json({ "status": "Niste unijeli novu grupu za studenta!" });
            return;
        }

        let novaGrupa = parametri['grupa'];
        Grupa.findAll({ where: { naziv: novaGrupa } }).then(grupe => {
            if (grupe.length == 0) {
                return Grupa.count().then(x => {
                    return Grupa.create({ naziv: parametri['grupa'] });
                })
            } else
                return Grupa.findOne({ where: { naziv: parametri['grupa'] } })
        }).then(grupa => {
            Student.update({ GrupaId: grupa.id }, { where: { id: studenti[0].id } }).then(() => {
                res.json({ "status": "Promjenjena grupa studentu " + indexStudenta });
            })
        })
    });

});

app.post('/batch/student', function(req, res) {
    let tekst = req.body;
    if (req.get("Content-Length") == 0) {
        //jer nece parsirati dobro inace
        tekst = '';
    };


    //posto je na win os-u novi red zapravo \r\n, uklonicemo \r ako ima, pa split izvrsiti po \n
    //eventualno cemo izbaciti viska prazne redove
    //tekst = tekst.replaceAll('\r', '');
    //posto ovo radi tek u node15+ verzijama, uradicemo split ovako, a potom opet izbaciti prazne redove
    var redovi = tekst.split(/[\r\n]/).filter(r => r.length > 0);

    //Provjera eventualno neispravnog csv-a
    let neispravanCSV = false;
    for (let i = 0; i < redovi.length; i++) {
        let podatak = redovi[i].split(',');
        if (podatak.length != 4) {
            neispravanCSV = true;
        }
    }
    if (neispravanCSV) {
        res.json({ "status": "Niste unijeli ispravan CSV format! Provjerite unesene podatke!" });
        return;
    }

    var nizStudenata = [];
    var nizIndeksa = [];
    for (let i = 0; i < redovi.length; i++) {
        let s = redovi[i].split(',');
        nizStudenata.push({ "ime": s[0], "prezime": s[1], "index": s[2], "grupa": s[3] })
        nizIndeksa.push(s[2]);
    }
    var nizStudBezDuplih = [];
    var nizIndeksaBezDuplih = [];
    var istiIndexi = [];
    nizStudenata.forEach(s => {
        if (!nizIndeksaBezDuplih.includes(s['index'])) {
            nizStudBezDuplih.push(s);
            nizIndeksaBezDuplih.push(s['index']);
        } else
            istiIndexi.push(s['index']);

    });


    var nizGrupa = [];
    var noveGrupe = [];
    var grupeBezPonavljanja = [];
    var grupaZaUnosObjekat = [];
    var sId = 0;
    var nizGresakaSortirano = [];
    Student.findAll({ where: { index: nizIndeksaBezDuplih } }).then(studs => {
        var nizGresaka = [];
        for (let i = 0; i < studs.length; i++) {
            nizGresaka.push(studs[i]['index']);
            nizStudBezDuplih.splice(nizStudBezDuplih.findIndex(a => a['index'] == studs[i]['index']), 1);
        }
        nizGresaka = nizGresaka.concat(istiIndexi);
        for (let i = nizIndeksa.length - 1; i >= 0; i--) {
            if (nizGresaka.includes(nizIndeksa[i])) {
                nizGresakaSortirano.push(nizIndeksa[i]);
                nizGresaka.splice(nizGresaka.indexOf(nizIndeksa[i]), 1);
            }
        }
        nizGresakaSortirano.reverse();
        //nizStud bez duplih sad fakat ima sve studente koje treba unijeti, a nizGresakaSortirano sve greske
        //potrebno naci sve grupe koje treba dodati
        for (let i = 0; i < nizStudBezDuplih.length; i++) {
            nizGrupa.push(Grupa.findAll({ where: { naziv: nizStudBezDuplih[i].grupa } }).then(grupe => {
                if (grupe.length == 0) {
                    noveGrupe.push(nizStudBezDuplih[i].grupa);
                }
            }))
        }
        return Promise.all(nizGrupa);
    }).then(() => {
        noveGrupe.forEach(element => {
            if (!grupeBezPonavljanja.includes(element))
                grupeBezPonavljanja.push(element);
        })
        return Grupa.count().then(x => {
            for (let i = 0; i < grupeBezPonavljanja.length; i++) {
                grupaZaUnosObjekat.push({ naziv: grupeBezPonavljanja[i] });
            }
        })
    }).then(() => {
        return Grupa.bulkCreate(grupaZaUnosObjekat);
    }).then(() => {
        return Student.count();
    }).then((br) => {
        sId = br;
        return Grupa.findAll();
    }).then(gr => {
        var napokonZaDodati = [];
        for (let i = 0; i < nizStudBezDuplih.length; i++) {
            let trazena = gr.filter(gg => gg['naziv'] == nizStudBezDuplih[i]['grupa'])
            napokonZaDodati.push({ "ime": nizStudBezDuplih[i].ime, "prezime": nizStudBezDuplih[i].prezime, "index": nizStudBezDuplih[i].index, "GrupaId": trazena[0].id });
        }
        return Student.bulkCreate(napokonZaDodati);
    }).then(() => {
        if (nizGresakaSortirano.length == 0)
            res.json({ "status": ("Dodano " + nizStudenata.length + " studenata!") });
        else {
            let porukaGreske = 'Dodano ' + (nizStudenata.length - nizGresakaSortirano.length) + " studenata, a studenti ";
            for (let i = 0; i < nizGresakaSortirano.length; i++) {
                if (i != nizGresakaSortirano.length - 1)
                    porukaGreske += nizGresakaSortirano[i] + ',';
                else
                    porukaGreske += nizGresakaSortirano[i];
            }
            porukaGreske += ' već postoje!';
            res.json({ "status": porukaGreske });
        }
    })
});

var server = app.listen(3000)
module.exports = server